function onClickEvent() {
  let myCount = 2;
  alert(`You completed ${myCount} exercise`);
  myCount = nestedCall(myCount);
  alert(`You completed ${myCount} exercise`);
}

function nestedCall(count) {
  document.querySelector("body > ul > li:nth-child(1)").remove();
  console.log("Reached nested call exercise");
  return count + 6;
}
